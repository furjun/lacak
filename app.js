var express = require("express");
var path = require("path");
var hbs = require("express-handlebars");
var bodyParser=require("body-parser");
var session = require('express-session');

var app = express();
var connection = require('./config');
app.use(session({
  secret: 'Ai83Km9dLksdai9z',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 600000 }
}));

app.use(function(req, res, next){
  if(req.session.Email != null) {
    app.engine("hbs", hbs({extname: "hbs", defaultLayout: "dashboard", layoutsDir: __dirname + "/layout"}));
  }
  else {
    app.engine("hbs", hbs({extname: "hbs", defaultLayout: "layout", layoutsDir: __dirname + "/layout"}));
  }
  next()
});

app.use(express.static('template'));

app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "views"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var routes = require('./routes/__routes');

app.use(routes);

app.use(function(err, req, res, next) {
  console.log(err.message);
  return res.render('layout/error', {
    title: 'Error '+res.statusCode,
    message: err.message,
    error: res.statusCode,
  });
});

// app.get('/', function(req, res) {
//   res.render('dashboard', {title: 'Hello'});
// });

app.listen(3000, function() {
  console.log('ready on port 3000');
});

// app.use(require('./listen'));
