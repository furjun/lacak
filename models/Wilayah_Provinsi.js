const Sequelize = require("sequelize");
const db = require("../db");

module.exports = db.sequelize.define(
  "ID-Kota", {
    Id: {
      type: Sequelize.VARCHAR,
      primaryKey: true,
      allowNull: false,
    },
    Nama: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  }, {
    timestamps: false,
    freezeTableName: true,
  }
);
