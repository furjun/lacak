const Sequelize = require("sequelize");
const db = require("../db");

module.exports = db.sequelize.define(
  "User", {
    Nama: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Email: {
      type: Sequelize.STRING,
      primaryKey: true,
      allowNull: false,
    },
    Password: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Tanggal: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
    Verifikasi: {
      type: Sequelize.STRING,
    }
  }, {
    timestamps: false,
    freezeTableName: true,
  }
);
