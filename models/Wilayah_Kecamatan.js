const Sequelize = require("sequelize");
const db = require("../db");

module.exports = db.sequelize.define(
  "ID-Kecamatan", {
    Id: {
      type: Sequelize.VARCHAR,
      primaryKey: true,
      allowNull: false,
    },
    Kota: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Nama: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  }, {
    timestamps: false,
    freezeTableName: true,
  }
);
