const Sequelize = require("sequelize");
const db = require("../db");

module.exports = db.sequelize.define(
  "LACAKER-Device", {
    User: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Id: {
      type: Sequelize.STRING,
      primaryKey: true,
      allowNull: false,
    },
    Nama: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Latitude: {
      type: Sequelize.STRING,
      // allowNull: false,
    },
    Longitude: {
      type: Sequelize.STRING,
      // allowNull: false,
    },
    Mesin: {
      type: Sequelize.TINYINT,
      allowNull: false,
    },
    Speed: {
      type: Sequelize.STRING,
      // allowNull: false,
    },
    Pusat: {
      type: Sequelize.STRING,
      // allowNull: false,
    },
    Jarak: {
      type: Sequelize.STRING,
      // allowNull: false,
    },
    Waktu: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    }
  }, {
    timestamps: false,
    freezeTableName: true,
  }
);
