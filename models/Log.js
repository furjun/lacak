const Sequelize = require("sequelize");
const db = require("../db");

module.exports = db.sequelize.define(
  "LACAKER-Device", {
    Lacaker: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Latitude: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Longitude: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Speed: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Time: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    }
  }, {
    timestamps: false,
    freezeTableName: true,
  }
);
