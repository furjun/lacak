var express = require("express");

var app = express();

const devices = require('../controllers/devices');

app.get(['/devices', '/devices/index'], isLogin, devices.index);

app.route('/devices/create').get(isLogin, devices.create.get).post(isLogin, devices.create.post);

app.route('/devices/edit/:id').get(isLogin, devices.edit.get).post(isLogin, devices.edit.post);

app.get('/devices/delete/:id', isLogin, devices.delete);

app.get('/devices/:id', isLogin, devices.view);

 module.exports = app;
