var express = require("express");

var app = express();

const site = require('../controllers/site');

app.get(['/', '/site', '/site/index'], site.index);
app.get('/site/about', site.about);
app.get('/site/contact', site.contact);

 module.exports = app;
