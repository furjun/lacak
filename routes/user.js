const express = require("express");

const app = express();

const user = require('../controllers/user');

app.get(['/user', '/user/index'], isLogin, user.index);

app.route('/user/register').get(user.register.get).post(user.register.post);

app.route('/user/login').get(user.login.validator, user.login.get).post(user.login.post);

app.get('/logout', isLogin, user.logout);

app.get('/user/view', isLogin, user.view);

app.get('/user/:email', isLogin, user.view);

 module.exports = app;
