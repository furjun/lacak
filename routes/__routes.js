var express = require("express");

var app = express();

app.use(require('../routes/dashboard'));
app.use(require('../routes/devices'));
app.use(require('../routes/map'));
app.use(require('../routes/site'));
app.use(require('../routes/user'));

module.exports = app;
