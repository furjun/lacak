var express = require("express");

var app = express();

const map = require('../controllers/map');

app.get(['/map', '/map/index'], isLogin, map.index);

 module.exports = app;
