var express = require("express");

var app = express();

const dashboard = require('../controllers/dashboard');

app.get(['/dashboard', '/dashboard/index'], isLogin, dashboard.index);

 module.exports = app;
