var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '!@#webmaxindo#@!',
  database : 'lacaker'
});
connection.connect(function(err){
if(!err) {
    console.log("Database is connected");
    global.db = connection;
} else {
    console.log("Error while connecting with database");
}
});

var isLogin = function(req, res, next) {
  if (req.session.Email == null) {
    res.redirect('/user/login');
  }
  else {
    next();
  }
}

global.isLogin = isLogin;

module.exports = connection;
