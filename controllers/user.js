const User = require("../models/User");
const sha1 = require('sha1');

const user = {
  index: function(req, res) {
    return data = db.query("SELECT Nama, Email, Tanggal FROM User", function(err, resi, filed) {
      if (err) throw err;
      // console.log(res);
      res.render('user/index', {title: 'User', data: resi});
      // return res;
    });
  },
  register: {
    get: function(req, res) {
      res.render('user/register', {title: 'User Register'});
    },
    post: function(req, res) {
       var message = '';

       if(req.method == "POST"){
          var post  = req.body;
          var nama = post.name;
          var email= post.email;
          var pass= post.password;
          const data = {
            Nama: nama,
            Email: email,
            Password: sha1(pass),
            Verifikasi: Math.floor(100000 + Math.random() * 900000),
          }

          User.findOne({
            where: {
              Email: email,
              Password: sha1(pass),
            }
          }).then(results => {
            if (!results) {
              User.create(data).then(results => {
                message = "Succesfully! Your account has been created.";
                console.log(message);
                res.redirect('/user');
              }).catch(err => {
                throw err;
              });
            }
            else {
              res.render('user/register', {title: 'User Register', message: message});
            }
          }).catch(err => {
            throw err;
          });
       } else {
          res.render('user/register', {title: 'User Register', message: message});
       }
    }
  },
  login: {
    validator: function(req, res, next) {
      if (req.session.Email != null) {
        res.redirect('/dashboard');
      }
      else {
        next();
      }
    },
    get: function(req, res) {
      res.render('user/login', {title: 'User Login'});
    },
    post: function(req, res) {
       var message = '';

       if(req.method == "POST"){
          var post  = req.body;
          var email= post.email;
          var pass= post.password;

          User.findOne({
            where: {
              Email: email,
              Password: sha1(pass),
            }
          }).then(results => {
            if(results){
              // console.log(results._options);
              req.session.Email = email;
              res.redirect('/dashboard');
            }
            else{
              message = 'Wrong Credentials.';
              res.render('user/login', {title: 'User Login', message: message});
            }
          }).catch(err => {
             res.render('user/login', {title: 'User Login', message: message});
          });

       } else {
          res.render('user/login', {title: 'User Login', message: message});
       }
    }
  },
  logout: function(req, res, next) {
    if (req.session) {
      // delete session object
      req.session.destroy(function(err) {
        if(err) {
          return next(err);
        } else {
          return res.redirect('/dashboard');
        }
      });
    }
  },
  view: function(req, res) {
    var sql = "SELECT Nama, Email, Tanggal FROM User WHERE Email = ?";
    var data = db.query(sql, [req.params.email ? req.params.email : req.session.Email], function(err, resi, filed) {
      if (err) throw err;
      res.render('user/view', {title: 'User Profil', data: resi[0]});
      // return res;
    });
  },
}

 module.exports = user;
