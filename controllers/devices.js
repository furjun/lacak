const Devices = require("../models/Devices");

const devices = {
  index: function(req, res) {
    Devices.findAll({
      where: {
        User: req.session.Email,
      }
    }).then(results => {
      res.render('devices/index', {title: 'Devices', data: results});
    }).catch(err => {
      throw err;
    });
  },
  create: {
    get: function(req, res) {
      res.render('devices/create', {title: 'Create Device'});
    },
    post: function(req, res) {
       var message = '';

       if(req.method == "POST"){
          var post  = req.body;
          var Id = post.Id;
          var Nama = post.Nama;
          var Mesin = post.Mesin;
          Devices.create({
            User: req.session.Email,
            Id: Id,
            Nama: Nama,
            Mesin: Mesin
          }).then(results => {
            message = "Succesfully! Your device has been created.";
            console.log(message);
            res.redirect('/devices');
          }).catch(err => {
            throw err;
          });
       } else {
          res.render('devices/create', {title: 'Create Dev', message: message});
       }
    }
  },
  edit: {
    get: function(req, res) {
      Devices.findOne({
        where: {
          User: req.session.Email,
          Id: req.params.id,
        }
      }).then(results => {
        res.render('devices/edit', {title: 'Edit Device', data: results});
      }).catch(err => {
        throw err;
      });
    },
    post: function(req, res) {
       var message = '';

       if(req.method == "POST"){
          var post  = req.body;
          var Nama = post.Nama;
          var Mesin = post.Mesin;
          Devices.findOne({
            where: {
              User: req.session.Email,
              Id: req.params.id,
            }
          }).then(results => {
            results.Nama = Nama;
            results.Mesin = Mesin;
            results.save().then(results => {
               message = "Succesfully! Your device has been updated.";
               console.log(message);
               res.redirect('/devices');
            }).catch(err => {
              throw err;
            });
          }).catch(err => {
            throw err;
          });
       } else {
          res.render('devices/edit', {title: 'Create Dev', message: message});
       }
    }
  },
  delete: function(req, res) {
    Devices.findOne({
      where: {
        User: req.session.Email,
        Id: req.params.id,
      }
    }).then(results => {
      results.destroy().then((results) => {
        message = "Succesfully! Your device has been deleted.";
        console.log(message);
        res.redirect('/devices');
      })
    }).catch(err => {
      throw err;
    });
  },
  view: function(req, res) {
    Devices.findOne({
      where: {
        User: req.session.Email,
        Id: req.params.id,
      }
    }).then(results => {
      res.render('devices/view', {title: 'View Device', data: results});
    }).catch(err => {
      throw err;
    });
  }
}

 module.exports = devices;
