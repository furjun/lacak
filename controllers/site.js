const site = {
  index: function(req, res) {
    res.render('site/index', {title: 'Home'});
  },
  about: function(req, res) {
    res.render('site/about', {title: 'About'});
  },
  contact: function(req, res) {
    res.render('site/contact', {title: 'Contact'});
  }
}

module.exports = site;
